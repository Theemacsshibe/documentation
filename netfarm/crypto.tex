\section{Cryptographic operations}

\subsection{Hashing}

Netfarm uses cryptographic \concept{hashes} to ``name'' objects, so that they
can be referenced and retrieved by clients that do not already have them
stored. Hashes are also used in the process of signing objects.

An object is hashed by rendering it in the binary format, and then
hashing the resulting octet vector using the SHA-256 hash algorithm. Usually,
computed values are not included in the render, as computed values must not
affect object identity. Furthermore, when hashes are used in signing objects,
signatures are not included, as they would need to be already present in the
object to be signed, or break identity (by changing the resultant hash, or by
allowing signatures to be added or removed to an object while preserving hash
value).

\Defun {hash-object} {object \key (emit-signatures \cl{t})}

Produce a binary hash. Note that inbuilt objects cannot be hashed, and thus
hashing them will signal an error. \cl{emit-signatures} will control if
signatures are included in the hash.

The most obvious inbuilt object that could not possibly be hashed is
\cl{inbuilt@schema}, which is its own schema. Of course, you could hash it by
referencing its inbuilt name and not its hash, but the renderer would have to
have a special case to use inbuilt objects' names instead of the result of
\cl{hash-object*}, which felt worse than disallowing hashing inbuilt objects
somehow.

\Defun {hash-object*} {object}

Produce a textual hash, equivalent to \cl{(bytes->base64 (hash-object object))}
for non-inbuilt objects, and in the form \cl{inbuilt@\textit{name}} for inbuilt
objects.

\Defmacro {with-hash-cache} {() \body body}

Cache hashes produced with both hashing functions in the body.

Hashing can result in accidentally quadratic runtime\footnote{We don't want to
  end up on \url{https://accidentallyquadratic.tumblr.com/}!} when traversing
graphs of objects and hashing them (such as saving a graph of objects with the
Netfarm client). \cl{with-hash-cache} will reduce hashing operations that have
already been done (usually by recursive calls between hashing and the renderer)
to hash-table lookups, reducing that kind of pattern to roughly linear runtime.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\linewidth]{quadratic-hashing.png}
  \caption{Hashing a graph of objects naively requires $ O(n^2) $ hashes}
\end{figure}

Hashes used in signing are currently not cached (\todo{though they probably
  should be}.)

If objects that have been hashed are modified, then the results of
\cl{hash-object} and \cl{hash-object*} are undefined; but the dynamic extent of
\cl{with-hash-cache} should allow for fairly easy analysis of where hashes can
be cached.

Object hashes are stored in a \term{weak hash table}, so they will be removed
when the objects are garbage, and the garbage collector runs.

\Defmacro {with-lexical-hash-cache} {() \body body}

Bind the hash caches in a way that allows for them to be closed over, and
restored using the local macro \cl{with-restored-lexical-hash-cache}.
This is intended to be used to share cached hashes between threads; which
takes some more deliberation to ensure correctness, but can be beneficial.

The Netfarm server uses a hash cache shared between all threads; and this is
safe as objects are never mutated inside the server.

\Defmacro {with-restored-lexical-hash-cache} {() \body body}

Restore the hash caches from \cl{with-lexical-hash-cache}.

\subsection{Signing}
\section{External formats}

Objects are usually \concept{serialized} in order to transmit them to another
client that does not share memory with the origin of the object, and that
client must then \concept{deserialize} the serialized form to produce a usable
object.

In Netfarm, serialization is done by converting an object to a \concept{vague
  object}, which has as much context of the object removed as possible,
which can then be serialized into a character or octet vector. These vectors
can then be deserialized by deserializing the vector into a vague object, and
then re-introducing the context that the origin of the object supplied.

The octet-vector representation is also used for generating and verifying
\term{signatures}.

\subsection{Vague objects}

\Defclass {vague-object}

The class of a \term{vague object}.

\Defun {apply-class} {vague-object class}

Recontextualize a \term{vague object} by ``applying'' a class to it, producing
an object.

\subsection{Character format}

\Defun {render} {value \optional stream}
 
Render a value. If a stream is given, then write the value to that stream, else
return the rendered value as a string.

\Defun {render-object} {object \optional stream}

Render an object. If a stream is given, then write the object to that stream, else
return the rendered object as a string.

\Defun {parse} {stream-or-string}

Parse a value from a stream or a string.

\Defun {parse-block} {string \key source name}

Parse an object from a string. The returned vague object has the supplied
\term{source} and name.

\subsection{Binary format}

\begin{figure}[H]
  \centering
  \begin{tabular}{r l l}
    byte       & ::= & \texttt{00} $\hdots$ \texttt{ff} \\
    byte-count & ::= & byte \\
    \hline
    integer & ::= & byte-count byte* \\
    length  & ::= & integer \\
    \hline
    string           & ::= & \texttt{01} length byte* \\
    byte-vector      & ::= & \texttt{02} length byte* \\
    positive-integer & ::= & \texttt{03} integer \\
    negative-integer & ::= & \texttt{04} integer \\
    list             & ::= & \texttt{05} length value* \\
    reference        & ::= & \texttt{06} length byte* \\
    true             & ::= & \texttt{07} \\
    false            & ::= & \texttt{08} \\
    slot-unbound     & ::= & \texttt{09} \\
    \hline
    value            & ::= & string $\mid$ byte-vector $\mid$ positive-integer \\
                     &     & $\mid$ negative-integer $\mid$ list  \\
                     &     & $\mid$ reference $\mid$ true $\mid$ false \\
    slot             & ::= & value $\mid$ slot-unbound \\
    slot-vector      & ::= & length slot* \\
    \hline
    metadata-element & ::= & string $\mid$ value \\
    metadata         & ::= & length metadata-element* \\
    \hline
    slots            & ::= & slot-vector \\
    computed-slots   & ::= & slot-vector \\ 
    object           & ::= & metadata slots computed-slots
  \end{tabular} \\
  Strings, byte vectors, slot vectors and references have as many bytes as the
  length represents, lists have as many values as the length represents, and
  metadata has as many elements as the length represents.

  The integer 0 must be represented as a positive-integer, and integers must
  be represented with as few bytes as possible; for example, 12,345 must be
  represented as \texttt{03 02 30 39}, and not \texttt{03 03 00 30 39} or with
  more zero bytes.
  \caption{An \term{Extended Backus-Naur form}-like syntax description of the
    Netfarm object format}
\end{figure}

\Defun {binary-render} {value \optional function}

Render a value. If a function is given, then write the value by calling it with
octet vectors that it should write, else return the rendered value as an octet
vector.

\Defun {binary-render-object} {object \key function
  (emit-computed-values \cl{t}) (emit-signatures \cl{t})}

Render an object. If a function is given, then write the value by calling it
with octet vectors that it should write, else return the rendered value as an
octet vector.

\cl{emit-computed-values} and \cl{emit-signatures} control if computed values
and signatures will be rendered, respectively. The resulting vector will not be
parseable if computed values and/or signatures are not rendered. These arguments
are present to facilitate hashing and signing, and usually should be left to
their defaults.

\Defun {binary-parse} {function-or-vector}

Parse a value from a function (called with no arguments, which returns octets)
or a vector.

\Defun {binary-parse-block} {function-or-vector \key source name}

Parse an object from a function (as previously described for \cl{binary-parse})
or a vector.
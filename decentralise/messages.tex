\section{Messages}

A \concept{message} is an abstract object that is sent over a \term{connection},
and can be considered the lowest-level communication protocol decentralise2 is
concerned with.

\Defmacro {message-case} {message \body case*}

\begin{center}
\begin{tabular}{r l l}
  case      & ::= & \texttt{(} pattern form* \texttt{)} \\
  pattern   & ::= & \texttt{(} keyword variable* \texttt{)} \\
  dont-care & ::= & $\sim$ $\mid$ \texttt{-} $\mid$ \texttt{\_} $\mid$ \texttt{NIL} \\
  variable  & ::= & dont-care $\mid$ symbol 
\end{tabular}
\end{center}

Match the value of \cl{message} against some patterns, which look like
the arguments to the function \cl{message}, and evaluate the forms of the
first matching case, with all variables that don't match the
rule \textsl{dont-care} bound to the arguments.

\Defun {message} {keyword \rest value*}

Create a message from the type designated by the keyword with the given values.
The number of values must be the same as the number of accessors of the type.

\subsection{Built in message types}

\newcommand{\Defmtype}[3] {\Dodocf {#1} {(\cl{#2} #3)} {Message~Type}}

\Defmtype {get-blocks}           {:get}                {names}
\Defmtype {put-block}            {:block}              {name version channels data}
\Defmtype {ok-response}          {:ok}                 {name}
\Defmtype {error-response}       {:error}              {name reason}
\Defmtype {new-subscriptions}    {:subscription}       {name version channels}
\Defmtype {subscription}         {:subscribe}          {channels}
\Defmtype {announcement-control} {:allow-announcement} {allow?}
\Defmtype {node-announcement}    {:announce}           {uri id}

\subsection{Defining message types}

\Defmacro {define-message-type} {keyword type-specifier constructor-name \rest accessors}

Define a message type named by the given keyword, which corresponds to the given
Common Lisp type specifier, can be constructed by calling the function named by
the \cl{constructor-name}, and can be destructured using all the functions named
in \cl{accessors}.

\subsection{Translators}

The content of messages (particuarly \cl{:block} messages) is often subject to
\concept{translation}, so that logic in a system can be separated cleanly from
serializing and deserializing for connections.

\Defgeneric {object->data} {object target source}

``Serialize'' or ``render'' an object (which came from, or represents an instance
of the source), into a value that can be sent as data on the target.

\Defgeneric {data->object} {data source target}

``Deserialize'' or ``parse'' the data (which represents an instance of, or
something the target can consume), into an object, which was received from the
source.

Note that all three arguments should be specialized, else weird things can occur.
For example, an \cl{ansible-connection} may be both a \cl{binary-connection} and
a \cl{character-connection}, so it is possible it will receive either character
or binary messages. If you write methods to parse some data with specializers
\cl{(t binary-connection target)} and \cl{(t character-connection target)},
either method may be selected, based on the class precedence list of
\cl{ansible-connection}, independent of the type of data in the message; and the
wrong parser may be used. Instead, you should specialize on
\cl{(string character-connection target)} and
\cl{(vector binary-connection target)}, so that the correct method will be
selected.
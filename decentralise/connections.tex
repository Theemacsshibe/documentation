\section{Connections}

A \concept{connection} connects a \term{client} and a \term{server} together,
allowing the two to exchange \term{messages} between each other. decentralise2
is able to handle many different types of connections, including in-memory
\term{passing connections} and \term{socketed connections} that use the
operating system's networking capabilities, which only have to implement a
simple protocol.

\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{decentralise2-connection-classes.png}
  \caption{A diagram of provided connection classes. Shaded classes are
    instantiable, and not shaded classes are not instantiable. Dashed
    classes are used to denote connection types.}
  \label{fig:connection-classes}
\end{figure}

\Defprotoclass {connection}

Note that a connection is implicitly started when it is instantiated.

\Defgeneric {stop-connection} {connection}
Stop the connection, which should free any resources that creating the
connection acquired, such as threads and sockets. This generic function
uses the \cl{progn} method combination, and thus every method should be
qualified with \cl{progn} (or \cl{:around}).

\Defgeneric {handle-message} {connection message}
Handle a message that was sent to the connection.

\Defmethod {handle-message} {(connection connection) message}
The default method calls the \term{message handler} of the connection with
the message.

\Definitarg {:message-handler}
\Defaccessor {connection-message-handler} {connection}

The \concept{message handler} is a function that should be called with
each received message. The default handler will wait for a new handler
to be set, in order to prevent some race conditions, where a message
could be received before the client has set a message handler.

\Defgeneric {write-message} {connection message}

Write a message to a connection. The implementation of this must be thread safe,
such that two threads can call \cl{write-message} simultaneously, and will
correctly write both messages.

\subsection{Connection types}

The types of data that a connection can send are represented by a connection's
class, not unlike the stream classes such as \cl{fundamental-character-stream}
and \cl{fundamental-binary-stream} in the Gray streams de-facto standard.

Note that, unlike Gray streams, all connections are expected to be
bidirectional, but a connection that drops all outgoing messages, or
never receives any incoming messages is arguably still correct.

\Defprotoclass {character-connection}

A connection that can send blocks with vectors of characters (strings) as data.

\Defprotoclass {binary-connection}

A connection that can send blocks with vectors of octets as data.

\subsection{Threaded connections}

\Defprotoclass {threaded-connection}

A connection that uses a thread to read messages.

\Defgeneric {read-message} {connection}

Read a message from the connection, blocking until one is present, then
returning it and \cl{T}, or return something and \cl{NIL} if a message
cannot be read ever again (e.g the other node closed the connection).

\Defgeneric {listener-loop} {connection}

The thread of the connection will call this function, and stop when it returns.

\Defmethod {listener-loop} {(connection threaded-connection)}

Repeatedly calls \cl{read-message}, handling its return values appropriately.

\subsubsection{Passing connections}

A \concept{passing connection} passes its messages to another connection,
sending messages that are written to the connection to the target. Two of
these connections in a loop can be used to create a \concept{hidden socket}.

Hidden sockets are very useful for testing, as they function entirely in the
Lisp image, without acquiring external resources or having to encode and decode
any messages.

\Defclass {passing-connection}

\Definitarg {:target}
\Defaccessor {passing-connection-target} {connection}

The connection that the passing connection should pass its messages to.

\Definitarg {:direction}
\Defreader {passing-connection-direction} {connection}

The ``direction'' of a passing connection. Any object that provides context to
the use of a connection may be supplied, and this object is printed when
printing a passing connection.

\Definitarg {:name}

The ``name'' of a passing connection, a string which provides context to the
use of a connection. This defaults to \cl{"Passing-Connection"}, and is printed
when printing a passing connection.

\Defun {make-hidden-socket} {\key class name initargs}

Create two passing connections that are each others' targets. The connections
are instances of the provided class (defaulting to \cl{passing-connection}),
and are instantiated with the provided initargs.

\subsection{Netfarm wire format connection}

\Defclass {netfarm-format-connection}

A \term{character connection} that exchanges messages using a simple textual
format, based on the wire protocol \term{Nettle} used.

As the name suggests, this was intended to be the default connection class for
Netfarm; but the creation of Netfarm's much more efficient binary format
suggests that we should use a binary connection class as default.

Nonetheless, it is quite easy to read, and superficially looks like the HTTP/1.0
wire protocol. Messages begin with unique ``verbs'', and are separated by new
lines.

\begin{figure}[H]
  \centering
  \begin{tabular}{r l l}
    boolean                    & ::= & \texttt{yes} $\mid$ \texttt{no} \\
    integer                    & ::= & (\texttt{0} $\hdots$ \texttt{9})* \\
    length-prefixed-string     & ::= & integer\ \texttt{:}\ character* \\
    \hline
    block-name         & ::= & length-prefixed-string \\
    channel-name       & ::= & length-prefixed-string \\
    id                 & ::= & length-prefixed-string \\
    line-count         & ::= & integer \\
    reason             & ::= & length-prefixed-string \\
    uri                & ::= & length-prefixed-string \\
    version            & ::= & integer \\
    \hline
    announce           & ::= & \texttt{announce} boolean \\
    block-header       & ::= & \texttt{block} block-name version line-count channel-name* \\
    error              & ::= & \texttt{error} block-name reason \\
    get                & ::= & \texttt{get} block-name* \\
    ok                 & ::= & \texttt{ok} block-name \\
    node               & ::= & \texttt{node} uri id \\
    subscribe          & ::= & \texttt{subscribe} channel-name* \\
    subscription       & ::= & \texttt{subscription} block-name version channel-name*
  \end{tabular} \\
  Length prefixed strings are preceded by the length of the string (written in
  base 10), and a colon.
  \label{fig:netfarm-wire-format}
  \caption{An \term{Extended Backus-Naur form}-like syntax description of the
    Netfarm wire format}
\end{figure}

The base decentralise2 messages are represented as follows:

\begin{itemize}
\item \cl{(:get names)} is represented by the rule \textsl{get}, with
  \textsl{block-name*} being all of \cl{names}.
  
\item \cl{(:block block-name version channels data)} is mostly represented by the rule
  \textsl{block-header}.
  Let \textsl{line-count} be the number of lines in \cl{data} (0 if \cl{data}
  is empty, or one more than the count of \cl{\#\textbackslash{}Newline}
  characters in \cl{data} otherwise).
  The rule \textsl{block-header} is used, with \textsl{channel-name*} being all
  of \cl{channels}, and other rules matched to variables with the same names.
  \textsl{line-count} lines of \cl{data} then succeed the block header.
  
\item \cl{(:ok name)} and \cl{(:error name reason)} are represented by the rules
  \textsl{ok} and \textsl{error} respectively trivially.
  
\item \cl{(:subscription name version channels)} is represented by the rule
  \textsl{subscription} mostly trivially, with \textsl{channel-name*} being all
  of \cl{channels}.

\item \cl{(:subscribe channels)} is represented by the rule \textsl{subscribe},
  with \textsl{channel-name*} being all of \cl{channels}.

\item \cl{(:allow-announcement allow?)} is represented by the rule
  \textsl{announce}, with \textsl{boolean} being \texttt{yes} if \cl{allow?} is
  true, or \texttt{no} if it is false.

\item \cl{(:announce uri id)} is represented by the rule \textsl{node}
  trivially.
\end{itemize}

\subsection{Netfarm binary connection}

\begin{figure}[H]
  \centering
  \begin{tabular}{r l l}
    boolean       & ::= & \texttt{00} $\mid$ \texttt{01} \\
    byte          & ::= & \texttt{00} $\hdots$ \texttt{ff} \\
    long-integer  & ::= & short-integer $ \times $ 8 \\
    short-integer & ::= & byte \\
    long-bytes    & ::= & long-integer byte* \\
    short-bytes   & ::= & short-integer byte* \\
    long-string   & ::= & long-bytes \\
    short-string  & ::= & short-bytes \\
    name-list     & ::= & long-integer short-string* \\
    \hline
    block-name    & ::= & short-string \\
    block-list    & ::= & name-list \\
    channel-list  & ::= & name-list \\
    version       & ::= & long-integer \\
    metadata      & ::= & block-name long-integer channel-list \\
    uri           & ::= & short-string \\
    id            & ::= & short-string \\
    \hline
    get                & ::= & \texttt{01} block-list \\
    character-block    & ::= & \texttt{02} metadata long-string \\
    binary-block       & ::= & \texttt{03} metadata long-bytes \\
    ok                 & ::= & \texttt{04} block-name \\
    error              & ::= & \texttt{05} block-name long-string \\
    subscribe          & ::= & \texttt{06} block-list \\
    subscription       & ::= & \texttt{07} metadata \\
    allow-announcement & ::= & \texttt{08} boolean \\
    announce           & ::= & \texttt{09} short-string short-string
  \end{tabular} \\
  Byte vectors are preceded by the length of the vector. Strings are byte
  vectors, that are decoded and encoded as character vectors. Name lists are
  lists of names, that are preceded by the length of the list.
  \label{fig:netfarm-binary}
  \caption{An \term{Extended Backus-Naur form}-like syntax description of the
    Netfarm binary wire format}
\end{figure}
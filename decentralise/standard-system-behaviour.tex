\section{Standard system behaviour}

\subsection{Message interpretation}

\begin{itemize}
\item \cl{:block}: If the name of the block provided is the name of a
  special block, the \cl{:put} function for that special block is called.
  Otherwise, if the system already has stored a block with the given name,
  and the version of the block provided is not greater than the version
  of the block stored by the system, an error with reason \cl{too old} is
  sent. If the provided block is otherwise newer, then it is stored using
  \cl{put-block}, and its metadata is broadcast to the appropriate
  connections.
  
\item \cl{:get}: For each name provided, the system either responds with
  a corresponding \cl{:block} message with the contents of a block, or
  an \cl{:error} message with the same name, and some reason as to why
  the block could not be retrieved.

  If a block names a special block, the \cl{:get} function for the
  special block is called. Otherwise, the block data is retrieved using
  \cl{get-block}.

  In either case, a \cl{:ok} message is sent if no error is signalled.
  Otherwise, \cl{describe-decentralise-error} is called to produce a
  reason string, and a \cl{:error} message is sent instead.
  
\item \cl{:allow-announcement}: If the message prevents announcement,
  then information about the connection will no longer appear in
  \cl{nodes} listings. If the message allows announcement, but the
  connection has not provided an ID, an \cl{:error} with reason
  \cl{need id first} is sent. If the message allows announcement, and the
  connection has provided an ID, then information about the connection will
  appear in \cl{nodes} listings.
\item \cl{:subscribe}:
\item \cl{:subscription}:
\end{itemize}

\todo{A subclass of \cl{standard-system} could handle \cl{:announce} in
      order to implement \term{peer discovery}.}

\subsection{Special blocks}

\newcommand{\Defspecialblock}[1]{\Dodocv{#1}{Special block}}

Retrieving and storing (using \cl{:get} and \cl{:block} messages,
respectively, with the names of) some blocks from a \term{standard
  system} will cause them to perform some special behaviour that does
not invoke \cl{get-block} or \cl{put-block} as per normal.

\Defmacro{define-special-block}
         {(class-name block-name \optional (data-type-name \cl{t}))
           \key get put}

Define a special block for instances of a class, with associated get and
put functions.

\note{Because we use generic functions to implement special block
      dispatch, it is possible to use \cl{call-next-method}.
      Should this be standard behaviour?}

The data returned by the get function is translated by
\cl{object->data}, with the given \cl{data-type-name} as source, and
the connection as target. The data given as an argument to the put
function is translated by \cl{data->object}, with the connection as
source, and the given \cl{data-type-name} as target.

The standard system provides some special blocks:

\Defspecialblock{list}

This block contains a list of every block name stored (excluding special
block names).

On a character connection, the list is encoded with the metadata of a
block per line, each line containing the length-prefixed name, the
version number, and then length-prefixed channels, somewhat mirroring
the syntax of the header of \cl{:block} messages when sent from a
\cl{netfarm-format-connection}.

On a binary connection, the list is encoded as repeating metadata
records. These records do not have any delimiters between them; as
their length can be detected from the record data. Each record
contains the name encoded as a short-string, the version as a
long-integer, the number of channels as a long-integer, then each
channel name as a short-string.

More formally, both encodings can be generated with the following grammars:

\begin{figure}[H]
  \centering
  \textbf{On character connections (building upon the rules in Figure
    \ref{fig:netfarm-wire-format}):}
  \begin{tabular}{r c l}
    block-metadata & ::= & block-name version channel-name* \\
    block-listing  & ::= & empty-string \\
                   &  |  & (block-metadata newline)* block-metadata
  \end{tabular}

  \textbf{On binary connections (building upon the rules in Figure
    \ref{fig:netfarm-binary}):}
  \begin{tabular}{r c l}
    metadata-record & ::= & block-name version channel-list \\
    block-listing   & ::= & metadata-record*
  \end{tabular}
  \caption{\term{Extended Backus-Naur form}-like syntax descriptions of listings}
\end{figure}

When a standard system receives a listing, it assumes that the blocks in
the listings may be retrieved from the client, and it may send \cl{:get}
requests for those blocks. If the listing cannot be parsed, a system will
instead respond with an \cl{:error} message, for the block \cl{list}, and
with the reason \cl{invalid listing}.

\Defspecialblock{id}

This block contains the ID of the node.

On a character connection, the ID is encoded as a 64-character hexadecimal
string, with uppercase \cl{A} through \cl{F} characters, and the most
significant digit written first. For example, the ID \cl{9781484261347}
would be encoded as
\cl{000000000000000000000000000000000000000000000000000008E56DE50FE3}.

\todo{Add a binary encoding. Currently, all our binary connection classes
  are also character connections, and are thus capable of sending character
  blocks, but this may not be ideal. On the other hand, a 50\% reduction
  of the size of a 64-byte block that is only sent once is not anything to
  write home about.}

When a standard system receives an ID, it associates that ID with the
connection used to send the ID. This ID appears in node listings, should
the connection then allow itself to be \term{announced}.

\Defspecialblock{nodes}

This block contains a listing of all nodes that have announced themselves
to the system, with each node record separated by a newline. Each item
in the listing has a length-prefixed URI, which may be used to connect to
a node, a space, and its ID.

\todo{Add a binary encoding. Again, this block is only retrieved once (as
  nodes are announced after that with another message type), but it would
  still be nice to have a binary encoding.}
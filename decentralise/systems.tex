\section{Systems}

A \concept{system} handles the requests sent to a node in a distributed
system, and sends requests for blocks it has not yet retrieved to synchronise
itself. A \concept{block} is a tuple consisting of a \textsl{name},
\textsl{version}, \textsl{channel set}, and some \textsl{data}; of which the
\concept{metadata} can be used by a system to determine what it must retrieve
and update.

\Defprotoclass {system}

The protocol class for a \term{system}. A system is maintained by some
\concept{maintenance threads}. One of which is the \concept{leader thread},
which runs a \concept{leader loop} that schedules requests for synchronisation
and stops itself when the system is stopped.

\todo{The standard-system should track maintenance threads like it tracks the
  leader thread, and eventually, this should all be integrated into the
  \textsl{bailout} thread supervision library.}

\Defclass {standard-system}

A system that uses the database and synchronisation protocols to implement the
basic behaviour of a node.

\Defclass {echo-system}

A silly system that responds to any messages other than invalid syntax with
the message sent.

\Definitarg {:id}

\Defgeneric {system-id} {system}

The ID of a system, as an integer in the range $[0, 2^{256})$

\Definitarg {:acceptors}

The acceptors of a system.

\subsection{Database protocol}

The database protocol is part of the system protocol, because a system may
process the data sent over the wire into some other format that should be
stored and serialised by some other protocol. For example, the Netfarm system
implements \cl{get-block} in terms of \cl{netfarm-system:get-vague-object},
and provides the rendered vague object and all the object names that affected
it via computed values.

It is possible to separate the synchronisation and database protocol
implementations, and combine them into a complete system implementation using
\textsl{multiple inheritance} when using the reference implementation:

\begin{lstlisting}[caption=Using multiple inheritance to create a system]
(defclass foobar-sql-database ()
  ((database-host :initarg :host
                  :reader foobar-database-host)
   ...))

(defclass my-system
  (decentralise-system:standard-system
  foobar-sql-database)
  ())

(defvar *system*
 (make-instance 'my-system
  ; an initarg for FOOBAR-SQL-DATABASE
  :host "sql-server.local"
  ; an initarg for STANDARD-SYSTEM
  :concurrent-requests 20))
\end{lstlisting}

\Defclass {memory-database-mixin}

The simplest database implementation, which stores block data in a
concurrent hash table.

\Defcondition {not-found}

A condition type that is raised when \texttt{get-block} or
\texttt{get-block-metadata} are called with the name of a block that is not
stored by the system.

\textbf{The following generic functions must be implemented by a database:}

  \Defgeneric {get-block} {system name}
  
  Return block structure for the block named \textit{name} from the
  \textit{system}, or signal a condition of type \cl{not-found}.
  
  \Defgeneric {put-block} {system name version channels data}
  
  Store a block with the provided block structure in the \textit{system}, or
  signal a condition of type \cl{error}.
  
  \Defgeneric {map-blocks} {function system}
  
  Call \textit{function} repeatedly with block structure for the metadata of
  every object in the \textit{system}.
  
  \Defgeneric {get-block-metadata} {system name}
  
  Return block structure for the metadata of the block named \textit{name} from
  the \textit{system}, or signal a condition of type \cl{not-found}.
  
  \textbf{This generic function will be called for each block's metadata sent
    to a system, so adding a method which does not have to load a block's data
    is highly recommended.} Otherwise, for the implementors' convenience, the
  following method will be used:
  
  \Defmethod {get-block-metadata} {(system system) name}
  
  The default method for \cl{get-block-metadata} calls \cl{get-block}, and
  returns the metadata from the data it retrieved, but implementations which are
  even slightly concerned with efficiency should implement a method for
  \cl{get-block-metadata} that does not have to read the data for a block.

\subsection{Synchronisation protocol}

When metadata about a block is received, it is categorised into either the
\concept{interesting set} or the \concept{uninteresting set}, or discarded.

These two sets are represented by predicates, and changes between the sets
can be represented in several ways (see \textsl{Set specifiers} for a reference
of these set specifiers). The interesting and uninteresting set predicates must
usually remain \textsl{referentially transparent} and not change, to allow the
system to cache block metadata and schedule requests; but the sets can be
modified when the system is notified, using
\cl{update-system-for-new-interesting-block-predicate}.

\Defgeneric {interesting-block-p} {system name version channels}

Determine if a block should be requested, based on its metadata.

\Defmethod {interesting-block-p} {(system standard-system) name version channels}

Everything is interesting by default.

\Defgeneric {uninteresting-block-p} {system name version channels}

Determine if a block's metadata should be kept, in case it may become
interesting later, due to
\cl{update-system-for-new-interesting-block-predicate}.

\Defmethod {uninteresting-block-p} {(system standard-system) name version channels}

Everything that has a newer version than the system stores is uninteresting to
the system.

\Defgeneric {update-system-for-new-interesting-block-predicate} {system interesting-set uninteresting-set}

Update the system's cache of the interesting set, adding every member of the
\textit{interesting-set} to the \term{interesting set} and removing every
member of the \textit{uninteresting-set} from it.

\subsection{Leader loop protocol}

The leader loop of a system performs tasks that are not easy to delegate to a
connection, such as watching for requests that timed out, and replacing them
with new requests.

\Defgeneric {leader-loop} {system}

Run the \term{leader loop} for the \textit{system}. This is expected to return
only when the system is stopped.

\Defmethod {leader-loop} {(system standard-system)}

The leader loop of a standard-system implements the description we gave of a
leader loop.

\Defgeneric {start-system} {system}

Start a system, by starting all its acceptors and a leader thread.

\Defgeneric {stop-system} {system}

Stop a system by closing all its acceptors and connections, and advising
maintenance threads to stop.

\Defgeneric {system-stopping-p} {system}

Should the threads maintaining the system stop now?

\subsection{Tuning the standard-system}

\Definitarg {:scheduler-timeout}

The time (in seconds) that the scheduler should wait for a request to be
responded to. This defaults to 10 seconds.

\Definitarg {:scheduler-concurrent-requests}

The number of requests that the scheduler can wait for at a time. This
defaults to 80 requests.

\todo{It may be useful to add a(n optional) limit on how many requests can be
  sent to one connection at a time.}
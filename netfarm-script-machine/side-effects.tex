\section{Side effects}

An interpreter may produce \concept{side effects} to interact with other
objects. Currently, there is only one category of side effect: side
effects which modify the \concept{computed value sets} of an object.

The final state of an object is produced by applying all the applicable
side effects. Depending on the nature of the implementation, it may or
may not be appropriate to implement side effects as suggested.

The order of which side effects are applied does not affect the produced
object. It is for this reason that Netfarm exhibits \concept{strong
  eventual consistency}: how objects are synchronised does not affect
the computation performed, and so all nodes storing an object will
eventually converge on objects with the same values.

\newcommand{\Defeffect}[2]{\Dodocf {#1} {\normalfont #2} {Side effect}}

\subsection{Computed values}

We maintain a mapping of computed values to counts for every slot of
every object. When all side effects have been considered, the table is
traversed, and each computed value is added according to the count if
it is not negative, and no computed values are added when a count is
negative.

\Defeffect{add-computed-value}{\cl{:target} \cl{:name} \cl{:value}}

Increment the count of the given computed value.

\Defeffect{remove-computed-value}{\cl{:target} \cl{:name} \cl{:value}}

Decrement the count of the given computed value.

A related technique for implementing these effects, which is useful
when saving effects to a disk, is to add each effect to a log, and
then compute the computed values by applying side effects when loading
an object.

It is trivial to show that computed values exhibit strong eventual
consistency. For each relevant effect, we add 1 or add -1 to each
counter, and addition of numbers is commutative, i.e. effects can be
performed in any order to retrieve the same result. It is also possible
to remove effects that cancel out, and to compress the result into one
final state periodically. However, the objects-affecting graph would
have to be maintained outside the effect log for it to still be correct.
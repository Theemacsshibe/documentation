\section{The script machine}

The script machine is a stack machine with a representation of lexical
environments and automatic memory management. The major changes to the
\term{SECD machine} are additional registers for convenience, and to
facilitate method calls between multiple objects, and a more common
bytecode-based instruction format, rather than an instruction format
based on S-expressions.

\subsection{Structures}

The script machine has these registers:

\noindent
\begin{tabularx}{\linewidth}{|r|l|X|}
  \hline
  Variable & Name            & Purpose \\
  \hline
  S        & Store           & A stack containing intermediate data. \\
  E        & Environment     & The lexical environment, represented as a list of
                               vector ``frames''. \\
  P        & Program counter & The position the interpreter is reading instructions from. \\
  D        & Dump            & A stack telling the interpreter where to return to.  \\
  \hline
  G        & Globals         & A vector of global variables. \\
  C        & Control         & The script the interpreter is current reading instructions from. \\
  A        & Capabilities    & Flags that allow the machine to perform operations that
                               only make sense in some situations. \\
  O        & Self            & The current object, or the empty list, if we have not called a
                               method. \\
  $\mathrm{O}_2$ & Sender     & The last object, which was the value of $O$
                                before calling a method. \\
  F        & Side effects    & Effects that the interpreter is going to make
                               on the outside world. \\
  \hline
\end{tabularx}

A script has these values:

\noindent
\begin{tabularx}{\linewidth}{|r|l|X|}
  \hline
  Variable & Name            & Purpose \\
  \hline
  P        & Program         & An octet vector that encodes instructions to
                               execute. \\
  E        & Entry points    & A vector of descriptions for procedures, which
                               are combined with environments by some
                               instructions to produce procedures. \\
  M        & Methods         & A vector of descriptions for methods. \\
  G        & Initial globals & A vector of the initial values of global variables. \\
  \hline
\end{tabularx}

A procedure has these values:

\noindent
\begin{tabularx}{\linewidth}{|r|l|X|}
  \hline
  Variable & Name            & Purpose \\
  \hline
  C        & Control         & The script to execute instructions from. \\
  E        & Environment     & The environment to extend when called. \\
  $\mathrm{P}_0$ & Initial program counter & The program counter to start
                                             executing from. \\
  A        & Argument count  & The number of arguments that this procedure must
                               be called with. \\
  O        & Object          & The object that the procedure belongs to. \\
  \hline                             
\end{tabularx}

\subsection{Notation}

\newcommand{\secd}[4]{$\langle #1, #2, #3, #4 \rangle$}
\newcommand{\transition}[2]{\begin{center} #1 $\rightarrow$ #2 \end{center}}
\newcommand{\stackdesc}[2]{\begin{center} #1 --- #2 \end{center}}
\newcommand{\Definstruction}[3]{\outdent{$\Rightarrow$}{#1: \texttt{#2} #3 \rightalign{\mbox{\brac{Instruction}}}}}

We write the state of a running interpreter as the tuple
\secd{S}{E}{P}{D}. We sometimes reference the registers $G$, $C$, $A$, $O$ and
$O'$ outside the tuple, because they are seldom used by instructions, and so
they are just tedious to write most of the time.

We write the states of a halted interpreter as halt[S], should it halt
correctly, or error[message, cause] should it fail. (Only the data stack is
preserved when an interpreter stops.)

We write the effects of interpreting an instruction as
\transition{old state}{new state}
Should an instruction concern itself with only the Store, we may write
effects in the form
\stackdesc{$i_1 \hdots i_m$}{$o_1 \hdots o_n$}
(borrowing from Forth conventions) which can be translated to the other
notation as
\transition{\secd{(i_m \hdots i_1\ .\ S)}{E}{P}{D}}
           {\secd{(o_n \hdots o_1\ .\ S)}{E}{P + length}{D}}
where $length$ is the total length of the instruction. Note that the equivalent
transition has the stack prefixes ``reversed''; the last input is popped first,
and the first output is pushed first. In Forth, we may write a program
\texttt{3 2 -} ($\Rightarrow$ \texttt{1})
and the stack before evaluating \texttt{-} as a list would be \texttt{(2 3)}.
To maintain a natural argument order for the programmer, the machine
handles arguments ``backwards'' when described using lists.

To access a value of a structure without destructuring it, we use the notation
$S_A$ to get the value of the variable (slot) $A$ of $S$. We also use
subscripts to retrieve values from lists and vectors; $S_n$ denotes the $n$th
value in $S$. The first element of a sequence is retrieved with a zero
subscript, as with most programming languages; and retrieving an element that
would be past the last element of a sequence causes interpretation to fail.

\newcommand{\proc}[5]{\mathrm{proc}[#1, #2, #3, #4, #5]}

We create many procedures while running programs, so we write a procedure in
the form $ \proc{C}{E}{P_0}{A}{O} $.

\subsection{Execution}

The machine first reads the instruction to execute, by reading the $P$th octet
in $ C_P $, and then incrementing $P$. The instruction arguments are then read
the same way. Finally, a transition is chosen based on the state of the machine
at this point. Unless the machine has transitioned to a $halt$ state or unwound
entirely to a $error$ state, it then proceeds to execute another instruction.

\subsection{Environment}

\begin{align*}
  getenv[(F . \_), variable, 0] &= F_{variable} \\
  getenv[(\_ . E), variable, frame] &= getenv[E, variable, frame - 1]
\end{align*}

\Definstruction {1} {get-proc} {n}

\transition{\secd{S}{E}{P}{D}}
           {\secd{(\proc{C}{E}{P_0}{A}{O}\ .\ S)}{E}{P + 2}{D}}
where $(P_0\ A) = (C_E)_n$

This closes over the current environment, whereas \cl{get-proc*} does
not.

\Definstruction {2} {get-value} {n}

\transition{\secd{S}{E}{P}{D}}{\secd{(G_n\ .\ S)}{E}{P + 2}{D}}

\Definstruction {3} {set-value!} {n}

\transition{\secd{(value . S)}{E}{P}{D}}{\secd{S}{E}{P + 2}{D}}
where $G_n \leftarrow value$

\Definstruction {4} {get-env} {variable frame}

\transition{\secd{S}{E}{P}{D}}{\secd{S}{E}{P + 3}{(value . D)}}
where $value = getenv[E, variable, frame]$

\Definstruction {5} {set-env!} {variable frame}

\transition{\secd{(value\ .\ S)}{E}{P}{D}}{\secd{S}{E'}{P + 3}{D}}
where $getenv[E, variable, frame] \leftarrow value$

\Definstruction {6} {get-proc*} {n}

\transition{\secd{S}{E}{P}{D}}
           {\secd{(\proc{C}{()}{P_0}{A}{O}\ .\ S)}{E}{P + 2}{D}}
where $(P_0\ A) = (C_E)_n$

\Definstruction {11} {byte} {n}

\stackdesc{}{n}

\subsection{Control flow}

\Definstruction {8} {return} {}

\transition{\secd{S}{E}{P}{()}}{halt[$S$]}

\transition{\secd{S}{E}{P}{(normalframe[E', P', C', O'] . D)}}
           {\secd{S}{E'}{P'}{D}}
where $ O \leftarrow O', C \leftarrow C' $

\transition{\secd{S}{E}{P}{(methodframe[E', P', C', O', O_2', F', S] . D)}}
           {\secd{((\cl{"ok"}\ .\ S)\ .\ S')}{E'}{P'}{D}}
where $ O \leftarrow O', O_2 \leftarrow O_2', F \leftarrow F' $

This transition handles half of the ``isolation'' of method calls,
returning a value that is distinguishable from a value produced by
propagating an error (with $ unwind $).

\Definstruction {9} {call} {n}

\transition{\secd{(\proc{C'}{E}{P_0}{count}{O'}\ A_n \hdots A_1\ .\ S)}{E}{P}{D}}
           {\secd{S}{((A_1 \hdots A_n)\ .\ E')}{P_0}{(normalframe[E, P + 2, C, O]\ .\ D)}}
when $ count = n $, where $ O \leftarrow O', C \leftarrow C' $

Note that the return address must be placed at the instruction after the call
instruction.
           
\Definstruction {10} {tail-call} {n}

\transition{\secd{(\proc{C'}{E}{P_0}{count}{O'}\ A_n \hdots A_1\ .\ S)}{E}{P}{D}}
           {\secd{S}{((A_1 \hdots A_n)\ .\ E')}{P_0}{D}}
when $ count = n $, where $ O \leftarrow O', C \leftarrow C' $

\Definstruction {16} {jump-cond} {thenhigh thenlow elsehigh elselow}

\transition{\secd{(\mathtt{\#f} . S)}{E}{P}{D}}
           {\secd{S}{E}{P + 5 + else}{D}}
where $ else = elsehigh \times 256 + elselow $
\transition{\secd{(v . S)}{E}{P}{D}}
           {\secd{S}{E}{P + 5 + then}{D}}
where $ then = thenhigh \times 256 + thenlow, v \neq \mathtt{\#f} $

Jumps are relative to the end of the instruction, so jumping 0 bytes
basically does nothing.

\Definstruction {17} {jump} {high low}

\transition{\secd{S}{E}{P}{D}}
           {\secd{S}{E}{P + 3 + high \times 256 + low}{D}}

\Definstruction {19} {call-method} {n}

\transition{\secd{S}{E}{P}{(Name\ Object\ A_n \hdots A_1 \hdots .\ D)}}
           {\secd{()}{((Methods\ A_1 \hdots A_n))}{P_0}{(methodframe[E, P, C, O, O_2, F, S] . D)}}
where $ primary[(...\ .\ Methods)] = methods[Object, Name] $
then $ O \leftarrow Object, O_2 \leftarrow O $

\todo{How to compute $ methods $, and how to handle \cl{does-not-understand}
  methods.}

Every script of the class of an object contains a list of lists
$ (Name\ P_0\ A') $, where $A'$ is the number of ``real'' arguments.
Methods have an implicit ``next method list'' argument. When calling a method,
the next method list is the first argument, and every other method is shifted
over one, so the first argument appears in the second position in the new frame.

The global variable vectors for each $\langle$object, script$\rangle$ pair must
be the same. This ensures that one method on an object and one script can observe
the changes made by another method on that object and that script.

It is usually a good idea to cache computation of $ methods $; method caching
in old Smalltalk was a fairly cheap way to get a reasonable performance gain,
as approximately every method call requires dispatching. This is true to a
lesser extent for Netfarm code, which has fewer, but still many, method
calls.

\Definstruction {24} {error} {}

\transition{\secd{(cause\ message . S)}{E}{P}{D}}
           {error[$message$, $cause$]}
\subsection{Forthisms}

\Definstruction {34} {drop} {}

\stackdesc{x}{}

\Definstruction {35} {dup} {}

\stackdesc{x}{x x}

\Definstruction {36} {swap} {}

\stackdesc{x y}{y x}

\subsection{Operators}

\Definstruction {64} {cons} {}

\stackdesc{car cdr}{(car . cdr)}

cdr must be a list; we can't represent improper lists in Netfarm, so we
don't give the script machine improper lists.

\Definstruction {65} {car} {}

\stackdesc{(car . \_)}{car}

\Definstruction {66} {cdr} {}

\stackdesc{(\_ . cdr)}{cdr}

\Definstruction {67} {null} {}

\stackdesc{()}{\#t}
\stackdesc{\_}{\#f}

\Definstruction {68} {consp} {}

\stackdesc{(\_ . \_)}{\#t}
\stackdesc{\_}{\#f}

\Definstruction {69} {append} {}

\stackdesc{a b}{a++b}

\Definstruction {71} {equal} {}

\stackdesc{a b}{equal[a,b]}

\begin{align*}
  equal[&(a_a\ .\ a_d), & (b_a\ .\ b_d)] &= equal[a_a, b_a] \land equal[a_d, b_d] \\
  equal[&(),            & ()]            &= \mathtt{\#t} \\
  equal[&a \in Integer, & b \in Integer] &= a = b \\
  equal[&a \in String,  & b \in String]  &= a = b \\
  equal[&a \in Object,  & b \in Object]  &= hash[a] = hash[b] \\
  equal[&proc[a_a, a_e, a_c], & proc[b_a, b_e, b_c]] &= a_a = b_a \land
                                                      a_e = b_e \land
                                                      a_c = b_c \\
  equal[&\_,            & \_]            &= \mathtt{\#f}
\end{align*}

\Definstruction {72} {string=} {}

\stackdesc{a b}{equal[a,b]}
when $ a \in String, b \in String $

\Definstruction {73} {list} {n}

\stackdesc{$e_1 \hdots e_n$}{$(e_1 \hdots e_n)$}

\Definstruction {96} {+} {}
\stackdesc{a b}{a+b}

\Definstruction {97} {-} {}
\stackdesc{a b}{a-b}

\Definstruction {98} {*} {}
\stackdesc{a b}{$ a \times b $}

\Definstruction {99} {/} {}
\stackdesc{a b}{$ \lfloor \frac{a}{b} \rfloor $}

The results of division are rounded to the closest integer
below the actual value; i.e \cl{-5 2 /} $\Rightarrow -3$

\Definstruction {100} {abs} {}
\stackdesc{a}{$|a|$}

\Definstruction {112} {=} {}
\stackdesc{a b}{a = b}
when $ a \in Integer, b \in Integer $
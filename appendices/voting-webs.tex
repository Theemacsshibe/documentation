\chapter{A description of a voting-web system}

We have not thoroughly tested this voting-web system, but we should describe
it, because it is a very useful but infrequently applied technique for
moderating an online system. This system may be seen as an extension of
many \textsl{Usenet} clients' \textsl{kill lists}, which contain a set of
patterns that match posts the user does not want to see; but it can also be
used as a whitelist, or just to rank some objects in some order.

It is very important that a distributed system has a means of distributed
``moderation''. We wonder why distributed systems are built without
distributed moderation of some form, and we believe that using centralised
moderation on such a system would be an equivalent of the ``people's stick''
described in \cite{statism-and-anarchy}. Unlike some kinds of moderation in the
real world, we are not required to give computers some kind of ``objective''
truth, and we do not attempt that with Netfarm. Freeing ourselves from that
obligation allows us to approximate the desires of each user with greater
precision.

This system allows a user to maintain a map of ``facts'' that they assert to
be true or false. A \term{presentation script} may query the voting-web system
while rendering an object, to decide what related objects it should render, or
a client program may just refuse to render some presentation forms of an object
that has certain facts about it asserted.

In the absence of a fact asserted by the user, we believe it is possible to
determine the probability that the user would assert the fact, as a function of
some delegates' assertions. Suppose $ Pr(F \mathrel{\text{is true to}} U) $ is
the probability that a user $ U $ takes the fact $ F $ to be true, and that
$ \text{delegates}(U) $ denotes the set of delegates of the user $ U $. We can
define the probability to be:

\begin{align*}
  \text{Pr}(F \mathrel{\text{is true to}} U) &= 1 & \text{(if U asserts F to be true)} \\
  \text{Pr}(F \mathrel{\text{is true to}} U) &= 0 & \text{(if U asserts F to be false)} \\
  \text{Pr}(F \mathrel{\text{is true to}} U) &=
       \frac{\displaystyle \sum_{D \in \text{delegates}(U)} Pr(F \mathrel{\text{is true to}} D) \text{correlation}(U, D)}
            {\displaystyle \sum_{D \in \text{delegates}(U)} |\text{correlation}(U, D)|} &
\end{align*}

We also introduced a function $ \text{correlation}(U, D) $ which estimates the
correlation coefficient between two users, which we have appropriated from the
\term{memory-based collaborative filtering} techniques used in recommender
systems. This could be defined as:

\begin{align*}
\text{correlation}(U, D)   &= \frac{\displaystyle \sum_{F \in \text{facts}} \text{support}(U, f) \text{support}(D, f)}
                                   {\sqrt{\displaystyle \sum_{F \in \text{facts}} \text{support}(U, F)^2}
                                    \sqrt{\displaystyle \sum_{F \in \text{facts}} \text{support}(D, F)^2}} \\
\text{where support}(U, F) &= \begin{cases}
                                1  & \text{(if U asserts F to be true)} \\
                                -1 & \text{(if U asserts F to be false)} \\
                                0  & \text{(if U does not assert F)}
                              \end{cases} \\
\text{and facts}           &= \text{the rules U asserts} \cap \text{the rules D asserts}
\end{align*}

In the absence of a large number of common facts, setting the correlation
coefficient to be a small constant may yield results that are not very good,
but are better than not attempting to calculate a probability.

It may also be wise to bias the correlation and resultant probabilities towards
zero, to produce less confident values for fewer common facts, and fewer
delegates, respectively. This can be done by adding a small positive integer
to the denominators of our definitions of $ \text{correlation}(U, D) $ and
$ \text{Pr}(F \mathrel{\text{is true to}} U) $.
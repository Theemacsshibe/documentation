\chapter{A brief style guide}

How we write Common Lisp code is relatively uninteresting. We mostly
follow the SICL style guide
(chapter 30 of \url{http://metamodular.com/SICL/sicl-specification.pdf}),
with the exception of making package designators keywords. This is not
set in stone, and using uninterned symbol syntax (eg
\cl{\#:decentralise-system}) is also acceptable. Generally, we attempt
to use one file per \term{concept} (such as ``scheduling requests'',
or ``rendering an object to an octet vector''), and one package per
module (such as ``the client'' or ``the script machine'').

Most of our style is derived from the purposes we write code for, and
this is reflected in the protocols we write. There are some principles
behind how we decide on these protocols:

\section{Abstraction}

\textbf{Abstraction is \emph{not} the enemy! It is much easier to reason
  about an abstract protocol, than more concrete and lower-level
  constructs.}

We are not the only users of any layer of our system, so a
seemingly ``over-abstracted'' design may be beneficial for another user,
when someone else attempts to use a layer for a different
purpose. This is not something that can easily spiral out of control,
contrary to popular belief. As the layers of our Netfarm implementation
are completely separated, it is easy to observe what abstractions and
generalisations must be provided to support the other layers of this system.

Performing thought experiments and attempting to design systems for
other use cases may provide some insight into what features should be
included. However, it is likely that armchair philosophy won't provide
details which would only be seen by testing such a system; so
prototype it and watch what happens! If this is difficult to do, then
we have failed further in designing an easily adaptable system. The
contrapositive, however, is that designing an easily adaptable system
makes such experiments easy to do. This further provides insights
into how to design an easily adaptable system, and we benefit greatly
from this feedback loop.

\section{Feature ``creep''}

\textbf{What is the smallest feature set you can use to implement
  as much as possible? Then, what do you need to add to have users
  want to attempt as much as possible?}

We are not particuarly interested in keeping the system \term{simple},
because that tends to mean making \term{stupid} systems to many people.
Overspecialising the system, by prescribing a use case to the system,
such as presenting documents, or creating a chat application, is also
not desirable. To repeat part of the introduction, the aim is, by ``using
schemas and their scripts, it [should be] possible to implement many
different programs atop Netfarm.'' Implementing a paradigm or
program possible should be made possible at almost all costs, but making
implementation of one program easier, at the cost of making other programs
harder to implement, is never worth it.

If we need to remove a feature from a module, we must consider what
a user of that feature could use instead, and test if the resulting
designs are still as effective (or, in the case that the feature was
of poor quality, more effective). If the programming style is too
hampered by modifying a feature, it should not be modified, and an
alternative should be found.

For example, we took efforts to understand and analyse the uses of the
old client protocol before updating the protocol. The implementation
of the protocol in decentralise2 and the one known user (the Netfarm
client) were examined, and we compared the ergonomics and performance
of either protocol. Constructs were introduced where we couldn't
easily express features of the old protocol in the new, such as
introducing \cl{run-away} to run an action ``in the background'', which
would occur by creating a request but not waiting for its value,
and we also observed what code could be simplified by using features
of the new protocol, such as being able to generate actions just like
any other value, using loops and conditionals, as they are run
independently of being created.

\section{Portability}

\textbf{Ignoring all other connotations with the word \emph{modern},
  use any features that you would expect a \emph{modern} programming
  language to provide.}

There should be some concern towards portability, between
implementations and programming languages. The system should not be
based on ``magic'' constructs which can only be used in few
implementations or programming languages.

However, it is not a goal to support implementations which are
designed without useful constructs, such as
\term{automatic memory management} and \term{exception handling},
which greatly decrease the work required to implement a system that is
designed with them in mind. There are features in Netfarm which
require these constructs, such as the \term{script machine}, which
does not perform explicit memory management, and also has
\term{unwinding}, which is easier to program with exceptions; and most of
the object semantics demand being able to create multiple references
to an object, with non-trivial lifetimes.

Anything which is almost universally considered easy to compile today
could probably be expected. Counter-examples include
\cl{call-with-current-continuation} and \cl{fexpr}s, for which
new compilation techniques are being produced, but the performance is
rarely close to the performance of systems without these constructs.
(Also consider that most programming language implementations are old,
 and if they implement these constructs, they may or may not be using
 the latest techniques.)
Note that this is not related to wide-spread adoption, which is not
strictly required. For example, many so-called object-oriented
languages, such as C++ and Java, do not feature metaobject protocols,
and a convenient way to create new classes at runtime. Implementing
systems, such as the Common Lisp meta-object protocol, relatively
efficiently has been understood almost since it was introduced.
(Okay, compiling with \cl{call/cc} has been understood for
 a long time, notably with Steele's RABBIT Scheme compiler and Appel's
 book \emph{Compiling with Continuations}, but an efficient implementation
 of CLOS is not much slower than an efficient implementation of a worse
 object system, such as the Java object system.)
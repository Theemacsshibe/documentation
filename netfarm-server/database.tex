\section{Database protocol}

\subsection{Implementations of the database protocol}

\begin{figure}[t]
  \centering
  \includegraphics[width=\linewidth]{netfarm-server-classes.png}
  \caption{A diagram of Netfarm server classes, including system and
    database mixin classes. Shaded classes are instantiable, and not
    shaded classes are not instantiable. Dashed classes are decentralise2
    classes included for completion.}
  \label{fig:netfarm-server-classes}
\end{figure}

\Defclass {memory-system}

A system that stores everything in memory. Compare this to decentralise2's
\cl{memory-database-mixin}.

\Defclass {simple-memory-system}

A system that stores everything in memory, using the simplest feasible
implementations of the database protocol.

\Defclass {filesystem-mixin}

A mixin that stores objects in a filesystem.

Objects are stored in a ``trie'' constructed of directories, to avoid slowdowns
that occur on some file systems, should one directory contain many files.
For each object, three files are stored:
\begin{itemize}
\item A \texttt{.object} file that contains the object, and computed values
  appended to the end of the object,
\item A \texttt{.affected-by} file that contains a list of object names that
  this object has been affected by, and
\item A \texttt{.affects} file that contains a list of object names that this
  object affects.
\end{itemize}

Some files are also stored in the storage directory:
\begin{itemize}
\item A \texttt{dependency-graph} file that stores the \term{dependency graph} as an
  association list,
\item A \texttt{interesting-set} file that stores the \term{other interesting set},
\item A \texttt{unpresentable-set} file that stores the inverse of the
  \term{presentable set} (as it would be smaller than the presentable set),
\item \todo{A \texttt{fulfilled-dependencies} file that stores object names
    that have had all dependencies fulfilled, but have not been further
    processed}, and
\item \todo{A \texttt{script-machines} file that stores information about the
    script machines that need to be run.}
\end{itemize}

\Definitarg {:directory}

The directory that objects should be stored in.

\Defclass {caching-mixin}

A mixin that caches some objects in memory, but defers to another database
implementation when it does not have an object stored.

\Definitarg {:cache-size}

The maximum rendered size of objects that can be cached at once, in bytes.
This defaults to 100 megabytes ($ 10^8 $ bytes).

\Definitarg {:fallback-vague-object-size}

The size that vague objects should be assumed to take when rendered, should
the real database not provide a size.

\subsection{Vague object storage}

\Defgeneric {put-vague-object} {system vague-object}
\Defgeneric {get-vague-object} {system name}

\Defgeneric {map-vague-objects} {function system}
\Defgeneric {vague-object-stored-p} {system name}

\Defgeneric {count-vague-objects} {system}

\subsection{Presentable set}

\Defaccessor {presentable-name-p} {system name}
\Defgeneric {count-vague-objects} {system}

\subsection{Side effects}

\Defgeneric {apply-side-effect} {system name cause side-effect-type \key}
\Defgeneric {map-computed-values-caused-by} {function cause-name system}

\Defgeneric {objects-affected-by} {system name}
Return a list of the object hashes affected by a given object.

\Defgeneric {objects-affecting} {system name}
Return a list of the object hashes which affect a given object.

\Defgeneric {objects-affecting-hash} {system name}
Compute a hash value for the objects-affecting list, by
\[ \prod_{a \in \text{affecting}} a \pmod{2^{256}} \]
This can be implemented incrementally, but the default method computes
it from the entire objects-affecting list. (Recall multiplication and
modular multiplication are commutative - a database could store the
last hash, and then compute $ h' \equiv h \times a \pmod{2^{256}} $
when adding $ a $.)

(We were initially going to reduce \cl{logxor} over the set, but
bitwise exclusive-or, among all additive groups, can be attacked by
\cite{birthdayproblem}. It is unclear what such an attack could achieve,
but we are going to avoid it by using a multiplicative group instead.)

\subsection{Other interesting set}

\Defgeneric {add-other-interesting-block} {system name}
\Defgeneric {remove-other-interesting-block} {system name}
\Defgeneric {other-interesting-block-p} {system name}

\subsection{Dependency graph}

A Netfarm server maintains a \concept{dependency graph}, consisting of a set of
\concept{dependency edges}, which each store the name of a dependent (a string),
the name of a dependency (another string), and a type (a keyword).

For the avoidance of doubt (because we frequently got confused by the names):
when a relevant edge exists, storing a dependent object is prevented until a
dependency object is retrieved.

All functions that take edge types except for \cl{add-dependency-edge} also
accept the type \cl{:all} to retrieve all edges, regardless of type.

\Defgeneric {add-dependency-edge} {system type dependent dependency}
\Defgeneric {remove-dependents} {system dependency}
\Defgeneric {map-dependents} {function system type dependency}
\Defgeneric {map-dependencies} {function system type dependencies}
\Defgeneric {map-dependency-edges} {function system}
\Defgeneric {no-dependencies-p} {system type name}

\Defclass {dependency-list-mixin}

A mixin that implements a dependency graph by storing a list of its edges.
This mixin may be slower than \cl{dependency-table-mixin}, but it is
easier to read about, as it does not do any tricky indexing.

\Defclass {dependency-table-mixin}

A mixin that implements a dependency graph by storing the edges in hash
tables. This may look up dependencies and dependents more efficiently than
\cl{dependency-table-mixin}, especially with many dependencies to manage.
#!/bin/sh

htlatex netfarm-book.tex "htlatex,charset=utf-8" " -cunihtf -utf8"
tex '\def\filename{{netfarm-book}{idx}{4dx}{ind}} \input  idxmake.4ht' 
makeindex -o netfarm-book.ind netfarm-book.4dx
sed -i -e 's/%/\\%/g' netfarm-book.ind
bibtex netfarm-book
htlatex netfarm-book.tex "htlatex,charset=utf-8" " -cunihtf -utf8"
htlatex netfarm-book.tex "htlatex,charset=utf-8" " -cunihtf -utf8"
# Delete some weird tags amsmath spits out
sed -i -e 's/<center[^>]*>//g' -e 's/<div class="center-quotation">//g' -e 's%</center>%%g' netfarm-book.html 
